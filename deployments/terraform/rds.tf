resource "aws_db_subnet_group" "private_subnets" {
  name       = "private_subnets"
  subnet_ids = aws_subnet.private_subnets[*].id

  tags = {
    Name = "My DB subnet group"
  }
}

resource "random_password" "db_password" {
  length  = 16
  special = false
}

resource "aws_secretsmanager_secret" "db_password_secret" {
  name = "db-pass-${var.environment}"
}

resource "aws_secretsmanager_secret_version" "db_pass_val" {
  secret_id     = aws_secretsmanager_secret.db_password_secret.id
  secret_string = random_password.db_password.result
}

resource "aws_db_instance" "events_database" {
  allocated_storage = 10
  db_name           = "events_db"
  engine            = "postgres"
  engine_version    = "14.5"
  instance_class    = "db.t3.micro"

  // Not secure, just example
  username = "foo"
  password = aws_secretsmanager_secret_version.db_pass_val.secret_string
  // Not secure, just example

  skip_final_snapshot    = true
  db_subnet_group_name   = aws_db_subnet_group.private_subnets.name
  vpc_security_group_ids = [aws_security_group.postgres_database_sg.id]
}


resource "aws_security_group" "postgres_database_sg" {
  name        = "postgres_database_sg"
  description = "Allow "
  vpc_id      = aws_vpc.main.id

  ingress {
    description     = "Postgres access"
    from_port       = 5432
    to_port         = 5432
    protocol        = "tcp"
    security_groups = [aws_security_group.sg_lambda.id]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "Postgres security group"
  }
}
