resource "aws_lambda_function" "events_queue_lambda_handler" {
  filename         = "${path.module}/lambda/events_lambda/main.zip"
  function_name    = "events_queue_lambda_handler-${var.environment}"
  role             = aws_iam_role.iam_for_events_queue_lambda.arn
  handler          = "main"
  source_code_hash = filebase64sha256("${path.module}/lambda/events_lambda/main.zip")
  timeout          = 60

  runtime = "go1.x"

  vpc_config {
    subnet_ids = aws_subnet.private_subnets[*].id
    security_group_ids = [
      aws_security_group.sg_lambda.id
    ]
  }

  environment {
    variables = {
      // Master user and pass, not secure!
      DB_USER     = aws_db_instance.events_database.username
      // It's better to take it through aws_secretsmanager_secret!
      DB_PASSWORD = aws_secretsmanager_secret_version.db_pass_val.secret_string
      DB_ENDPOINT = aws_db_instance.events_database.endpoint
      DB_NAME     = aws_db_instance.events_database.name
    }
  }
}

resource "aws_security_group" "sg_lambda" {
  vpc_id = aws_vpc.main.id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_lambda_event_source_mapping" "events_lambda_handler" {
  event_source_arn = aws_sqs_queue.events_queue.arn
  function_name    = aws_lambda_function.events_queue_lambda_handler.arn

  depends_on = [
    aws_iam_role_policy_attachment.lambda_sqs_events_queue,
  ]
}

resource "aws_iam_role_policy_attachment" "iam_role_policy_attachment_lambda_vpc_access_execution" {
  role       = aws_iam_role.iam_for_events_queue_lambda.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaVPCAccessExecutionRole"
}
