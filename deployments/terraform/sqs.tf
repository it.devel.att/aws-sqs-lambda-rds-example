resource "aws_sqs_queue" "events_queue" {
  name                      = "events-queue-${var.environment}"
  max_message_size          = 262144
  message_retention_seconds = 86400
  receive_wait_time_seconds = 10

  tags = {
    environment = var.environment
  }
}