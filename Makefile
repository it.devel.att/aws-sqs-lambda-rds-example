TERRAFORM_DIR=deployments/terraform
EVENTS_LAMBDA_DIR=${TERRAFORM_DIR}/lambda/events_lambda

build-events-lambda:
	GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -o ${EVENTS_LAMBDA_DIR}/main cmd/events_lambda/main.go

zip-events-lambda:
	cd ${EVENTS_LAMBDA_DIR}/ ; rm main.zip | true && zip -j main.zip main && rm -r main

update-events-lambda:
	${MAKE} build-events-lambda
	${MAKE} zip-events-lambda
