package main

import (
	"context"
	"fmt"
	"os"

	_ "github.com/jackc/pgx/stdlib"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/jmoiron/sqlx"
)

type Handler struct {
	dbClient    *PostgresClient
	invokeCount int
}

var handler *Handler

type DBConfig struct {
	User     string
	Password string
	Endpoint string
	DBName   string
}

func init() {
	// This is better solution to fetch credentials
	// cfg, err := config.LoadDefaultConfig(context.TODO())
	// if err != nil {
	// 	panic("configuration error: " + err.Error())
	// }
	// authenticationToken, err := auth.BuildAuthToken(
	// 	context.TODO(),
	// 	"mydb.123456789012.us-east-1.rds.amazonaws.com:3306", // Database Endpoint (With Port)
	// 	"us-east-1", // AWS Region
	// 	"jane_doe",  // Database Account
	// 	cfg.Credentials,
	// )
	// if err != nil {
	// 	panic("failed to create authentication token: " + err.Error())
	// }

	dbUser := os.Getenv("DB_USER")
	dbPassword := os.Getenv("DB_PASSWORD")
	dbEndpoint := os.Getenv("DB_ENDPOINT")
	dbName := os.Getenv("DB_NAME")
	dbConfig := DBConfig{
		User:     dbUser,
		Password: dbPassword,
		Endpoint: dbEndpoint,
		DBName:   dbName,
	}

	psqlClient, err := NewPostgresClient(dbConfig)
	if err != nil {
		panic(err)
	}

	psqlClient.DB.MustExec(`
		CREATE TABLE IF NOT EXISTS events (
			id SERIAL PRIMARY KEY,
			data JSON
		);
	`)

	handler = &Handler{
		dbClient: psqlClient,
	}
}

func main() {
	lambda.Start(handler.Handle)
}

type Event struct {
	Data string `db:"data"`
}

func (h *Handler) Handle(ctx context.Context, sqsEvents events.SQSEvent) (int, error) {
	h.invokeCount += 1
	fmt.Printf("Received %v events\n", len(sqsEvents.Records))
	events := make([]Event, len(sqsEvents.Records))
	for i := range sqsEvents.Records {
		record := sqsEvents.Records[i]
		fmt.Printf("New event: %+v\n", record.Body)
		events[i] = Event{Data: record.Body}
	}
	err := h.insertEvents(ctx, events)
	if err == nil {
		fmt.Println("Successfully insert events")
	}
	return h.invokeCount, err
}

func (h *Handler) insertEvents(ctx context.Context, events []Event) error {
	_, err := h.dbClient.DB.NamedExecContext(ctx, `
	INSERT INTO events
		(data)
	VALUES
		(:data)`, events)
	if err != nil {
		return err
	}
	return nil
}

type PostgresClient struct {
	DB *sqlx.DB
}

// postgres://user:password@127.0.0.1:5432/db_name
const postgresConnectionString = "postgres://%v:%v@%v/%v"

func NewPostgresClient(cfg DBConfig) (*PostgresClient, error) {
	connectionURL := fmt.Sprintf(postgresConnectionString, cfg.User, cfg.Password, cfg.Endpoint, cfg.DBName)
	client, err := sqlx.Connect("pgx", connectionURL)
	if err != nil {
		return nil, err
	}
	if err := client.Ping(); err != nil {
		return nil, fmt.Errorf("error while ping to postgres")
	}
	return &PostgresClient{client}, nil
}

func (client *PostgresClient) Close() error {
	return client.DB.Close()
}
